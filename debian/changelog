dired-rsync (0.7-3) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.5.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 25 Jul 2024 18:00:11 +0900

dired-rsync (0.7-2) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.3.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 24 Jul 2024 21:52:49 +0900

dired-rsync (0.7-1) unstable; urgency=medium

  * New upstream version 0.7
  * Add patch to fix version number
  * Refresh patch to clean documentation
  * Install dired-rsync-transient.el
  * d/control: Declare Standards-Version 4.6.2 (no changes needed)
  * d/copyright: Bump copyright years

 -- Lev Lamberov <dogsleg@debian.org>  Thu, 20 Jul 2023 12:46:01 +0500

dired-rsync (0.6-1) unstable; urgency=medium

  * New upstream version 0.6
  * Add upstream metadata
  * Run upstream tests and keep tests file for autopkgtest
  * Do not install ERT tests to binary package
  * d/control: Add Testsuit field
  * d/control: Declare Standards-Version 4.5.1 (no changes needed)
  * d/control: Migrate to debhelper-compat 13
  * d/copyright: Update copyright years
  * d/rules: Clean targets

 -- Lev Lamberov <dogsleg@debian.org>  Thu, 31 Dec 2020 11:14:42 +0500

dired-rsync (0.5-1) unstable; urgency=medium

  * New upstream version 0.5
  * Migrate to debhelper 12 without d/compat
  * Drop unneeded patch (0001-fix-version.diff)
  * Refresh patch to clean documentation
  * d/control: Declare Standards-Version 4.4.1 (no changes needed)
  * d/control: Drop emacs25 from Enhances
  * d/control: Add elpa-dash and elpa-s to Build-Depends
  * d/copyright: Bump copyright years
  * d/rules: Override dh_installchangelogs to install upstream NEWS file

 -- Lev Lamberov <dogsleg@debian.org>  Sun, 01 Dec 2019 20:24:17 +0500

dired-rsync (0.4-3) unstable; urgency=medium

  * Team upload.
  * Regenerate source package with quilt patches

 -- David Bremner <bremner@debian.org>  Sun, 25 Aug 2019 14:15:34 -0300

dired-rsync (0.4-2) unstable; urgency=medium

  * Team upload.
  * Rebuild with current dh-elpa

 -- David Bremner <bremner@debian.org>  Sat, 24 Aug 2019 19:54:09 -0300

dired-rsync (0.4-1) unstable; urgency=medium

  * New upstream version 0.4
  * Add patch to fix version number
  * Add patch to clean documentation
  * d/control: Declare Standards-Version 4.1.5 (no changes needed)
  * d/control: Add Rules-Requires-Root: no
  * d/control: Update Maintainer (to Debian Emacsen team)
  * d/rules: Override upstream build and install

 -- Lev Lamberov <dogsleg@debian.org>  Sun, 08 Jul 2018 14:09:11 +0500

dired-rsync (0.3-1) unstable; urgency=medium

  * New upstream version 0.3

 -- Lev Lamberov <dogsleg@debian.org>  Sat, 05 May 2018 14:06:34 +0500

dired-rsync (0.2-1) unstable; urgency=medium

  * Initial release (Closes: #896770)

 -- Lev Lamberov <dogsleg@debian.org>  Tue, 24 Apr 2018 13:38:09 +0500
